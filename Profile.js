import React, { Component } from 'react'
import { StatusBar, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Avatar } from 'react-native-elements';
import { Icon } from 'react-native-elements/dist/icons/Icon';
import LinearGradient from 'react-native-linear-gradient';

export default class Profile extends Component {

    render() {
        const profile = this.props.route.params
        const StatusBarHeight = StatusBar.currentHeight
        return (
            <>
                <View style={{ alignItems: "center" }}>
                    <View style={{ height: StatusBarHeight, width: '100%' }}>
                        <LinearGradient
                            style={style.Container}
                            colors={["#FF8040", "#FFD801"]}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}>
                            <StatusBar
                                barStyle="light-content"
                                translucent={true}
                                backgroundColor="transparent"
                            />
                        </LinearGradient>
                    </View>
                    <View style={{ height: 350, width: '100%' }}>
                        <LinearGradient
                            style={style.Container}
                            colors={["#FF8040", "#FFD801"]}
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}>
                            <Avatar size={120} rounded source={{ uri: profile.avatar, }} />
                        </LinearGradient>
                    </View>
                    <Text style={{ bottom: 105, color: "#fff", fontSize: 30, fontWeight: "bold" }}>{profile.first_name} {profile.last_name}</Text>
                    <View style={{ width: "100%", flexDirection: "row", justifyContent: "center" }}>
                        <LinearGradient style={style.Back} colors={["#E975A8", "#726CF8"]} useAngle={true} angle={150}>
                            <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => { this.props.navigation.goBack() }} >
                                <View style={{ backgroundColor: "#fff", width: 25, borderRadius: 50 }}>
                                    <Icon name="arrow-back" type="ionicon" />
                                </View>
                                <Text style={{ color: "#fff", fontSize: 18, fontWeight: "bold" }}>    Back To Home</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                </View>
                <View>
                    <View style={{ flexDirection: 'row', justifyContent: "space-between", alignItems: "center" }}>
                        <View style={{ paddingLeft: 25, paddingVertical: 10 }}><Icon name="email" type="fontisto" size={30} /></View>
                        <Text style={[style.itemStyle, { right: 60 }]}>
                            {profile.email}
                        </Text>
                        <Icon name="chevron-thin-right" type="entypo" size={16} />
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: "space-between", alignItems: "center" }}>
                        <View style={{ paddingLeft: 25, paddingVertical: 10 }}><Icon name="baidu" type="fontisto" size={30} /></View>
                        <Text style={[style.itemStyle, { right: 60 }]}>
                            {profile.id}
                        </Text>
                        <Icon name="chevron-thin-right" type="entypo" size={16} />
                    </View>
                </View>
            </>
        )
    } r
}

const style = StyleSheet.create({
    Container: {
        flex: 1,
        backgroundColor: '#2980b9',
        justifyContent: 'center',
        alignItems: 'center',
    },
    Back: {
        borderRadius: 50, bottom: 75, paddingVertical: 20, width: 250,
        backgroundColor: "red", flexDirection: 'row', justifyContent: "center", alignItems: 'center'
    },
    itemStyle: {
        fontSize: 16, fontWeight: "bold"
    }
})
